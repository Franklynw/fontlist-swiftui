//
//  CustomFont.swift
//  FontList
//
//  Created by Franklyn on 25/06/2019.
//  Copyright © 2019 Franklyn. All rights reserved.
//

import SwiftUI


struct CustomFont: ViewModifier {

    let fontName: String
    let fontSize: CGFloat?
    let textStyle: UIFont.TextStyle?

    init(fontName: String, textStyle: UIFont.TextStyle) {
        self.fontName = fontName
        self.textStyle = textStyle
        self.fontSize = nil
    }

    init(fontName: String, size: CGFloat) {
        self.fontName = fontName
        self.fontSize = size
        self.textStyle = nil
    }

    /// Will trigger the refresh of the view when the ContentSizeCategory changes.
    @Environment(\.sizeCategory) var sizeCategory: ContentSizeCategory

    func body(content: Content) -> some View {

        if let fontSize = self.fontSize {
            return content.font(.custom(self.fontName, size: fontSize))
        }

        guard let textStyle = self.textStyle, let size = self.fontSizes[textStyle] else {
            fatalError()
        }

        let fontMetrics = UIFontMetrics(forTextStyle: textStyle)
        let fontSize = fontMetrics.scaledValue(for: size)

        return content.font(.custom(self.fontName, size: fontSize))
    }


    private let fontSizes: [UIFont.TextStyle: CGFloat] = [
        .largeTitle: 34,
        .title1: 28,
        .title2: 22,
        .title3: 20,
        .headline: 17,
        .body: 17,
        .callout: 16,
        .subheadline: 15,
        .footnote: 13,
        .caption1: 12,
        .caption2: 11
    ]
}


extension View {

    func customFont(named fontName: String, style: UIFont.TextStyle = .body) -> Self.Modified<CustomFont> {
        return self.modifier(CustomFont(fontName: fontName, textStyle: style))
    }

    func customFont(named fontName: String, size: CGFloat) -> Self.Modified<CustomFont> {
        return self.modifier(CustomFont(fontName: fontName, size: size))
    }
}

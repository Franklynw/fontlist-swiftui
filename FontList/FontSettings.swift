//
//  FontSettings.swift
//  FontList
//
//  Created by Franklyn on 26/06/2019.
//  Copyright © 2019 Franklyn. All rights reserved.
//

import SwiftUI
import Combine


class FontSettings: BindableObject {

    static let defaultSize: CGFloat = 20

    var didChange = PassthroughSubject<Void, Never>()

    var excludedFontNames: [String] = [
        "Bodoni Ornaments",
        "Devanagari Sangam MN",
        "Heiti TC",
        "Kohinoor Devanagari",
        "Kohinoor Telugu",
        "Kannada Sangam MN",
        "Khmer Sangam MN",
        "Lao Sangam MN",
        "Malayalam Sangam MN",
        "Myanmar Sangam MN",
        "PingFang SC",
        "PingFang TC",
        "Sinhala Sangam MN",
        "Tamil Sangam MN"
    ]

    var name: String = "Helvetica" {
        didSet {
            self.didChange.send()
        }
    }
    var size: CGFloat = FontSettings.defaultSize {
        didSet {
            self.didChange.send()
        }
    }
    var isCustomSize: Bool = false {
        didSet {
            if self.isCustomSize == false {
                self.size = FontSettings.defaultSize
            }
            self.didChange.send()
        }
    }
}


extension View {

    func customFont(using settings: FontSettings) -> Self.Modified<CustomFont> {
        if settings.isCustomSize {
            return self.modifier(CustomFont(fontName: settings.name, size: settings.size))
        }
        return self.modifier(CustomFont(fontName: settings.name, textStyle: .body))
    }
}

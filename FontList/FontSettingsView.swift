//
//  FontSettingsView.swift
//  FontList
//
//  Created by Franklyn on 25/06/2019.
//  Copyright © 2019 Franklyn. All rights reserved.
//

import SwiftUI
import Combine


struct FontSettingsView : View {

    @ObjectBinding var settings: FontSettings

    var body: some View {
        NavigationView {
            ContentView(settings: settings)
                .navigationBarTitle(Text("Font"), displayMode: .inline)
        }
    }
}

struct ContentView: View {

    @State var settings: FontSettings

    var body: some View {
        VStack {

            HeaderView(settings: settings)

            FontListView(excludedFontNames: settings.excludedFontNames) { fontName in
                self.$settings.name.value = fontName
            }
        }
    }

    struct HeaderView: View {

        @State var settings: FontSettings

        var body: some View {
            VStack {

                HStack {
                    VStack (alignment: .leading) {
                        Text("Custom font size:")
                            .font(.body)
                        Text("If disabled, will use system settings")
                            .font(.caption)
                    }
                    Toggle(isOn: $settings.isCustomSize) {
                        Text("")
                    }
                }

                Slider(value: $settings.size, from: 10, through: 30, by: 0.1)
                    .disabled($settings.isCustomSize.value == false)

                Text("Current font: \($settings.name.value)")
                    .customFont(using: self.settings)
                    .padding()

            }.padding()
        }
    }

    struct FontListView: View {

        struct FontName: Identifiable {
            var id = UUID()
            let value: String
        }

        private let fontNames: [FontName]
        private let tapped: ((String) -> ())?

        init(excludedFontNames: [String] = [], tapped: ((String) -> ())?) {
            self.fontNames = UIFont.familyNames.sorted().filter({ excludedFontNames.contains($0) == false }).map { FontName(value: $0) }
            self.tapped = tapped
        }

        var body: some View {
            List(fontNames) { fontName in
                Button(action: {
                    self.tapped?(fontName.value)
                }) {
                    FontNameCell(fontName: fontName.value)
                }
            }
        }

        struct FontNameCell: View {

            let fontName: String

            var body: some View {
                HStack {
                    Text(fontName).customFont(named: fontName)
                    Spacer()
                }
            }
        }
    }
}



#if DEBUG
struct ContentView_Previews : PreviewProvider {
    static var previews: some View {
        FontSettingsView(settings: FontSettings())
    }
}
#endif
